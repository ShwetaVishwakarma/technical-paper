# How do browsers render HTML, CSS, JS to DOM?

The purpose of this article is to explain, how a browser renders HTML, CSS, and JavaScript to DOM and the mechanism behind it. 

Let's get started.

Initially, we need to understand

## What is a web browser?
 
The web is a vast and powerful tool. It is software that loads files or information from a server and displays it on your desktop or mobile device. It provides a user interface.

## How web browser figures out what to display?

The web browser is consists of software that itself figure out what to display on the screen based on the file it receives which is regarded as the browser engine.

### Sending and receiving information to the browser

Now let us move towards understanding how sending and receiving information in the browser takes place?

Before going into the mechanism of sending and receiving information through a web browser let us first understand the concept of data.

The data is information that has been translated into a form that is efficient for movement or processing. The data is transmitted in the form of packets sized in bytes.

Here I would like to clarify that when you write HTML, CSS, and JS and try to open the HTML file in your browser, then instead of reading them as it is the browser reads the raw bytes of HTML from the hard disc. Raw bytes of data is binary stream format which is a text file containing response header `Content-Type` set to the value `text/html, charset="UTF-8"`.

Here `text/html` tells the browser that it is an HTML document and Charset= UTF-8 tells the browser that it is encoded in **UTF-8** Chararcter encoding.

This cannot be understood by the browser. For making it useful for the browser you must first convert it into the form that it understands.

## Process of converting raw bytes of HTML to DOM

As we know firstly the raw bytes of data are converted into characters.

Here browser has received actual characters in the files but still, they are not the final result. Now, these characters are further parsed into tokens.

Without the process of tokenization, the bunch of characters is meaningless text for the browser engine.
When you save a file with `.html` extension you signal the browser engine to interpret the file as an HTML document and the browser interprets the file is by first parsing it. In this process and especially during tokenization, each starts and end HTML tag in the file is accounted for. 

The passer understands each string and sets of rules in angle brackets that apply to each of them.

At this point also tokens are not our final result. After the process of tokenization, the tokens are further converted into nodes but they still aren't the final result.

These nodes are then linked in a form of a tree data structure known as the [DOM](https://www.w3schools.com/js/js_htmldom.asp). The DOM forms the parent-child relationships, adjacent sibling relationships, etc. The relationship between every node is established in this DOM object.

Now, this is something which we can work with it.

<p align="center">
  <img width="460" height="250" src="https://blog.logrocket.com/wp-content/uploads/2018/09/image10.jpg">
</p>

## Learning about fetching CSS

The dom has been created.

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" media="screen" href="main.css">
    <title>Document</title>
</head>
<body>
</body>
</html>
```

While receiving the raw byte of data along with the DOM construction process, the browser simultaneously requests to fetch the `main.css` stylesheet linked.

### Process of converting raw bytes of CSS to CSSOM

Here also the raw byte of CSS data passes through the same process that is the conversion into the characters, then tokenization and then nodes formation, and finally a CSS tree structure is formed which is regarded as a CSS object model. 

<p align="center">
  <img width="460" height="250" src="https://blog.logrocket.com/wp-content/uploads/2018/09/image4.jpg">
</p>

CSS has something known as [cascade](https://www.w3schools.com/css/css_intro.asp) which shows the browser the type of styles applied to an element. The CSSOM plays an important role because the browser has to recursively go through the CSS tree structure for determining the style affecting a particular element.

## Render tree

Here we have two independent tree structures that are DOM and CSSOM. The DOM consists of all the information about the HTML pages while the CSSOM  has the information about the styles of the element.

<p align="center">
  <img width="460" height="250" src="https://blog.logrocket.com/wp-content/uploads/2018/09/image22.jpg">
</p>

Now the browser combines the DOM and CSSOM tree which is called render tree.
Now the browser advances on to the next step that is the layout.

## Layout

We do have the content and style information of all the visible content on the screen but we have rendered nothing onto the screen.

Firstly the browser has to go for the calculation of the exact size and position of each object on the page. This is the layout step which is also known as the reflow step. It takes into account the content and the style received from the DOM and CSSOM and performs all the necessary layout computing operations.

## Painting

Now the last step is to paint the elements to the screen.
With the information on the content (DOM), style (CSSOM), and the exact layout of the elements computed the browser now paints the individual node to the screen.

Finally, the elements are now rendered to the screen.

## JavaScript rendering

JavaScript is essential for a decent web application. With this, you can remove and add elements from the DOM tree and you may modify the CSSOM properties of an element also.

Whenever in JavaScript the browser encounters a script tag, the DOM construction process is halted until the script finishes its execution. This is because JavaScript can change both the DOM and CSSOM. Because the browser isn't sure what this particular JavaScript will do, it takes precautions by halting the entire DOM construction altogether.

Script tag can be introduced in two places of our HTML document, one within in body tag and the second one in the head tag.

let us look at them one by one

 ### Script tag at the bottom of the body tag

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
<body>
    <p id="header">Browser Rendering</p>
    <script>
        let header = document.getElementById(header);
        console.log(header);
    </script>
</body>
</html>
```
Within the script tag, I'm accessing the DOM for a node with `id` and `header` and then logging in to the console.

This works fine. The DOM operation was successful.

### Script tag at the head 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <script>
        let header = document.getElementById(header);
        console.log(header);
    </script>
    <title>Document</title>
</head>
<body>
    <p id="header">Browser Rendering</p>
</body>
</html>
```
Once I do this, the output in the console is `null`.

The DOM operation has failed because a `script` tag was encountered by the HTML parser in the process of constructing the DOM. At this time, the `body` tag and its content had not been parsed.

It is to be noted that the location of your script matters.
`<script>` tag can be written in the head tag or body tag depending on the condition.
If there is no connection between the HTML element and JavaScript then we can place the `script` tag in the head.
After loading the HTML element, if we want to perform some operation on that element then we have to place the `script` tag in the `body` tag.

The DOM construction will remain halted if you extract the inline `script` to an external local file. The DOM construction stops until an encountered script tag is found and but that's not the case with the CSSOM.

With CSSOM the JS execution waits. No CSSOM, no.JS execution.

## The async attribute

The DOM construction can be prevented from halting with the addition of the async keyword to the script tag.

Here's an example:

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <script src="http://some-link" async></script>
    <title>Document</title>
</head>
<body>
    <p id="header">Browser Rendering</p>
</body>
</html>
```

## References

1. What is a Web Browser?

- https://en.wikipedia.org/wiki/Web_browser

2. What is Browser Engine?

- https://en.wikipedia.org/wiki/Browser_engine

3. What is Data?

- https://www.tutorialspoint.com/computer_fundamentals/computer_data.htm

4. What is Cascade Style Sheet (CSS)?

- https://www.w3schools.com/css/css_intro.asp

5. What is Document Object Model (DOM)?

- https://www.w3schools.com/js/js_htmldom.asp

6. How does browser rendering works?

- https://www.browserstack.com/guide/browser-rendering-engine

- https://blog.logrocket.com/how-browser-rendering-works-behind-scenes/
